//
// Created by 孙方媛 on 3/27/18.
//

#ifndef EXCHANGE_MACHINE_TASKDISTRIBUTOR_H
#define EXCHANGE_MACHINE_TASKDISTRIBUTOR_H
#include<iostream>
#include<pthread.h>
#include<thread>
#include<string>
#include<queue>
#include<vector>
#include "httpSock.h"

template<typename TaskInfo>
class WorkerInfo{
public:
    WorkerInfo():thread(),task(){}
    pthread_t thread;
    TaskInfo task;
    bool isfree(){
        return task.isfree();
    }
};

template<typename T, typename Task>
class TaskDistributor{
public:
    TaskDistributor():uid(0){
        size_t n = std::thread::hardware_concurrency();
        n = n > 2 ? n - 2 : 2;
        for(size_t i = 0; i < n; i++){
            workers.push_back(WorkerInfo<Task>());
        }
        for(size_t i = 0; i < n; i++) {
            pthread_create(&workers[i].thread, NULL, requestHandler, &workers[i].task);
        }
    }

    TaskDistributor(size_t n):workers(n), uid(0){
        for(size_t i = 0; i < n;i++){
            pthread_create(&workers[i].thread, NULL, requestHandler, &workers[i].task);
        }
    }

  bool getTask(){
    while(1){
      for(auto& w : workers){
	if(w.isfree()){
	  Task task = fun();
	  task.uid = uid;
	  uid++;
	  w.task = task;
	}
      }
    }          
  }

private:
    std::vector<WorkerInfo<Task>> workers;
    size_t uid;
    T fun;
};

#endif //EXCHANGE_MACHINE_TASKDISTRIBUTOR_H
