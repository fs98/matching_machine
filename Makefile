CC=g++
OP=
FLAGS=-Wall -Werror -pedantic -std=gnu++11
LIBS=-lpthread
SOURCESERVER=server.cpp httpSock.cpp
SOURCECLIENT=client.cpp
DEP=taskDistributor.h httpSock.h

all:client server

client:$(SOURCE)
	$(CC) $(OP) $(FLAGS) $(SOURCECLIENT) -o client

server:$(SOURCE) $(DEP)
	$(CC) $(OP) $(FLAGS) $(SOURCESERVER) -o server $(LIBS)

clean:
	rm -f client server

sweep:
	rm *~ *.o
