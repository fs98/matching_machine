//
// Created by 孙方媛 on 3/27/18.
//
#ifndef EXCHANGE_MACHINE_HTTPSOCK_H
#define EXCHANGE_MACHINE_HTTPSOCK_H

#include <cstdlib>
#include <cstring>
#include <iostream>
#include <string>
#include <cstdio>
#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <pthread.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/select.h>
#include <string>
#include <sstream>
#include <list>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>

using namespace std;
using namespace boost::property_tree;

#define LISTEN_NUM 2000

enum class XML_type{create, transactions, invalid};

class TaskInfo{
public:
    TaskInfo():socket_fd(-1){}
    explicit TaskInfo(int fd, std::string ipAddr):socket_fd(fd), ipAddrString(ipAddr){}
    int socket_fd;
    size_t uid;
    std::string ipAddrString;
    bool isfree(){
        return socket_fd == -1;
    }
};

class Accept{
public:
    Accept():port("12345"){
        buildSock();
    }
    TaskInfo operator()();
    ~Accept(){
        close(server_fd);
    }

private:
    void buildSock();
    int server_fd;
    std::string port;
};

void* requestHandler(void* arg);

class operationInfo{
public:
    operationInfo(std::string _type, std::string _id, std::string _num):type(_type), id(_id), num(_num){}
    std::string type;
    std::string id;
    std::string num;
};


class HttpSock{
public:
    HttpSock():client_sock(-1), server_sock(-1){}
    HttpSock(int _client_sock, size_t _uid, std::string ip):client_sock(_client_sock), uid(_uid), ipAddrString(ip), server_sock(-1){}
    void buildTunnel(std::string client_ip);
    //void parse();
    void sendMsg(int socket_fd);
    void getMsg(int socket_fd);
    ~HttpSock(){
        if(client_sock != -1){
            close(client_sock);
        }
        if(server_sock != -1){
            close(server_sock);
        }
    }

    void recvl(int socket_fd, std::string& msg, size_t len, int flag);
    void recvh(int socket_fd, std::string& msg, int flag);
    void sendl(int socket_fd, std::string& msg, int flag);
    void getWholeMsg(int socket_fd);
    XML_type get_XML_type(std::string& msg);
    void parseXMLC(std::string& msg);
    void parseXMLT(std::string& msg);

    int client_sock;
    size_t uid;
    std::string ipAddrString;
    int server_sock;
    std::string msg;

    //@@@, <123456, 1000>--> sym, <123456, 100000>
    std::list<std::list<operationInfo> > fields;
};
#endif //EXCHANGE_MACHINE_HTTPSOCK_H
