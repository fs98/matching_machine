//
// Created by 孙方媛 on 3/27/18.
//
#include "httpSock.h"

#define BUF_SIZE 4096

// open server socket, make server listen
void Accept::buildSock() {
    int status;
    struct addrinfo host_info;
    struct addrinfo *host_info_list;
    const char *hostname = NULL;

    memset(&host_info, 0, sizeof(host_info));

    host_info.ai_family = AF_UNSPEC;
    host_info.ai_socktype = SOCK_STREAM;
    host_info.ai_flags = AI_PASSIVE;

    status = getaddrinfo(hostname, port.c_str(), &host_info, &host_info_list);

    if(status != 0){
        perror("");
        std::cerr << "Error: cannot get address info" << std::endl;
        //throw socketError(port.c_str());
    }

    for(auto it = host_info_list; it != NULL; it = it->ai_next) {
        server_fd = socket(it->ai_family, it->ai_socktype, it->ai_protocol);
        if (server_fd == -1) {
            continue;
        }

        int yes = 1;
        status = setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes));
        status = bind(server_fd, it->ai_addr, it->ai_addrlen);
        if (status == 0) {
            break;
        }
        close(server_fd);
    }

    if(status == -1){
        perror("");
        std::cerr << "Error: cannot create and bind socket" << std::endl;
        //throw socketError(port.c_str());
    }

    freeaddrinfo(host_info_list);

    status = listen(server_fd, LISTEN_NUM);
    if(status == -1){
        std::cerr << "Error: cannot listen on socket" << std::endl;
        //throw socketError(port.c_str());
    }
}

//Accept client and return the Task related with that client
TaskInfo Accept::operator()() {
    struct sockaddr_in socket_addr;
    socklen_t socket_addr_len = sizeof(socket_addr);

    int client_socket_fd = accept(server_fd, (struct sockaddr *)&socket_addr, &socket_addr_len);
    if(client_socket_fd == -1){
        perror("");
        std::cerr << "Error: cannot accept connection on socket" << std::endl;
        //throw acceptError();
    }

    char ipAddrC[INET_ADDRSTRLEN];

    //get the client's IP address in string form
    //inet_ntop - convert IPv4 and IPv6 addresses from binary to text form
    if(!inet_ntop(AF_INET, &(socket_addr.sin_addr), ipAddrC, INET_ADDRSTRLEN)){
        perror("inet_ntop() failed\n");
        exit(1);
    }

    std::string ipAddr(ipAddrC);
    TaskInfo t(client_socket_fd, ipAddr);
    return t;
}

//entry point of every thread
void* requestHandler(void* arg){
    TaskInfo* taskPool = static_cast<TaskInfo*>(arg);
    while(true){
        //not task to do, wait
        while(taskPool->isfree());

        TaskInfo task = *taskPool;
        //info the taskmaster to assign new task to do for next time
        taskPool->socket_fd = -1;

        HttpSock httpSock(task.socket_fd, task.uid, task.ipAddrString);

        httpSock.buildTunnel(task.ipAddrString);
    }
    return NULL;
}

void HttpSock::buildTunnel(std::string client_ip) {
    //may exist various exceptions
    getWholeMsg(client_sock);
    cout << "the whole msg: " << msg << endl;
    XML_type xml_type = get_XML_type(msg);
    if(xml_type == XML_type::create){
        parseXMLC(msg);
    }
    else if(xml_type == XML_type::transactions){
        parseXMLT(msg);
    }

    std::cout << "msg get from client: " << msg << std::endl;
}


//get whole msg by reading the len in the begin of msg
//after this, msg will store the whole msg from client
void HttpSock::getWholeMsg(int socket_fd){
    recvh(socket_fd, msg, 0);
    std::string len_s;
    size_t len;
    size_t found = msg.find('<');
    if(found != std::string::npos){
        len_s = msg.substr(0, found);
        //exception might exist here
        len = stoi(len_s);
        std::cout << "msgLen: " << len_s << std::endl;
    }

    msg = msg.substr(found);
    //std::cout << "real msg: " << msg << std::endl;
    if(msg.size() < len) {
        size_t len_remain = len - msg.size();
        recvl(socket_fd, msg, len_remain, 0);
    }
}


XML_type HttpSock::get_XML_type(std::string& msg){
    //may need to handle some exceptions here
    size_t found_c = msg.find("<create>");
    size_t found_t = msg.find("<transactions>");
    //the XML is a create XML
    if(found_c != std::string::npos){
        return XML_type::create;
    }
    else if(found_t != std::string::npos){
        return XML_type::transactions;
    }
    return XML_type::invalid;
}

void HttpSock::parseXMLC(std::string& msg){
    std::stringstream ss(msg);
    boost::property_tree::ptree pt;
    boost::property_tree::xml_parser::read_xml(ss, pt);
    BOOST_FOREACH(ptree::value_type &v1, pt.get_child("create")){
        if(v1.first == "account"){
            cout << "new account id= " << v1.second.get<string>("<xmlattr>.id") << endl;
            std::string type = "@@@";
            std::string id = v1.second.get<string>("<xmlattr>.id");
            std::string balance = v1.second.get<string>("<xmlattr>.balance");

            cout << "create account" << endl;
            cout << "id: " << id << endl;
            cout << "balance: " << balance << endl;

            operationInfo info = operationInfo(type, id, balance);
            std::list<operationInfo> toAdd;
            toAdd.push_back(info);
            fields.push_back(toAdd);
        }

        else if(v1.first == "symbol"){
            std::string type = v1.second.get<string>("<xmlattr>.sym");
            cout << "type: " << type << endl;
            std::string id;
            std::string num;
            std::list<operationInfo> toAdd;
            BOOST_FOREACH(ptree::value_type &v2, v1.second){
                if(v2.first != "<xmlattr>"){
                    id = v2.second.get<string>("<xmlattr>.id");
                    num = v2.second.data();
                    cout << "id: " << id << endl;
                    cout << "nums: " << num << endl;

                    operationInfo info = operationInfo(type, id, num);
                    toAdd.push_back(info);
                }
            }
            fields.push_back(toAdd);
        }
    }
}

void HttpSock::parseXMLT(std::string& msg){

}

void HttpSock::sendMsg(int socket_fd){
    sendl(socket_fd, msg, 0);
}

//receive BUF_SIZE length message
void HttpSock::recvh(int socket_fd, std::string& msg, int flag){
    char buf[BUF_SIZE];
    ssize_t recvl = recv(socket_fd, buf, BUF_SIZE, flag);
    if(recvl == -1){
        std::cerr << "recvError: socket_fd: " << socket_fd << std::endl;
        //throw recvError();
    }
    else if(recvl == 0){
        //throw socketClosed();
    }
    else{
        buf[recvl] = '\0';
        msg += std::string(buf, recvl);
    }
}

//receive given length message
void HttpSock::recvl(int socket_fd, std::string& msg, size_t len, int flag){
    char buf[BUF_SIZE];
    size_t recved = 0;
    while(len > recved){
        size_t recvLen = BUF_SIZE - 1 < (len - recved) ? BUF_SIZE - 1 : (len - recved);
        // std::cout << std::endl << "recvlen" << len - recved << std::endl << std::endl;
        ssize_t recvl = recv(socket_fd, buf, recvLen, flag);
        // std::cout << "buf: " << buf << std::endl << std::endl;
        if(recvl == -1){
            std::cerr << "recvError: socket_fd: " << socket_fd << std::endl;
            //throw recvError();
        }
        else if(recvl == 0){
            //throw socketClosed();
        }
        else{
            recved += static_cast<size_t>(recvl);
            buf[recvl] = '\0';
            msg += std::string(buf, recvl);
        }
    }
}

void HttpSock::sendl(int socket_fd, std::string& msg, int flag){
    size_t sent = 0;
    while(msg.size() > sent){
        // std::cout << "send length: " << sent << "/" << msg.size() << std::endl;
        ssize_t sentl = send(socket_fd, msg.c_str() + sent, msg.size() - sent, flag);
        if(sentl == -1){
            std::cerr << "sendError: socket_fd: " << socket_fd << std::endl;
            //throw sendError();
        }
        else{
            sent += static_cast<size_t>(sentl);
        }
    }
}


