#include "db_handler.h"

string format(string str){
  if(str[0]<='0'||str[0]>='9'){
    str.insert(0,"'");    
    for(int i = 1 ; i<str.size();i++){
      if(str[i] == '\''){
	str.insert(i,"'");
	i++;
      }
    }
    str+="'";
  }
  str+=",";
  return str;
}
string to_str(int &n){
  stringstream ss;
  ss<<n;
  string str;
  ss>>str;
  return str;
}
string to_str(long &n){
  stringstream ss;
  ss<<n;
  string str;
  ss>>str;
  return str;
}
string to_str(double &n){
  stringstream ss;
  ss<<n;
  string str;
  ss>>str;
  return str;
}
int to_int(string str){
  stringstream ss;
  ss<<str;
  int n;
  ss>>n;
  return n;
}
int add_account(connection *C,long account_id,double balance){
  string command ="INSERT INTO ACCOUNT (ACCOUNT_ID,BALANCE)"
    "VALUES (";
  command += format(to_str(account_id));
  command += to_str(balance);
  command +=") RETURNING ACCOUNT_ID;";
  result r;
  try{
    work W(*C);
    r=W.exec(command.c_str());
    W.commit();
  }catch(const std::exception &e){
    cerr <<"add_account: "<< e.what() <<std::endl;
    return -1;
  }
  return to_int(r[0][0].c_str());
}

int add_record(connection *C,  long trade_id, string sym, long num, double price,  long time){
  string command = "INSERT INTO RECORD (RECORD_ID,TRADE_ID,SYM,NUM,PRICE,TIME)"
    "VALUES ((SELECT CASE WHEN MAX(RECORD_ID) IS NULL THEN 1 ELSE MAX(RECORD_ID)+1 END FROM RECORD),";
  command += format(to_str(trade_id));
  command += format(sym);
  command += format(to_str(num));
  command += format(to_str(price));
  command += to_str(time);
  command +=");";
  try{
    work W(*C);
    W.exec(command.c_str());
    W.commit();
  }catch(const std::exception &e){
    cerr <<"add_record: "<< e.what() <<std::endl;
    return -1;
  }
  return 0;
}

int add_trade(connection *C, long trade_id, string sym, long account_id,  long num,  double price){
  string command = "INSERT INTO TRADE (ID,TRADE_ID,SYM,ACCOUNT_ID,LEFT_NUM,PRICE)"
    "VALUES ((SELECT CASE WHEN MAX(ID) IS NULL THEN 1 ELSE MAX(ID)+1 END FROM TRADE),";
  command += format(to_str(trade_id));
  command += format(sym);
  command += format(to_str(account_id));
  command += format(to_str(num));
  command += to_str(price);
  command +=");";
  try{
    work W(*C);
    W.exec(command.c_str());
    W.commit();
  }catch(const std::exception &e){
    cerr <<"add_trade: "<< e.what() <<std::endl;
  }
  return 0;
}

int delete_trade(connection *C, long trade_id){
  string command = "DELETE FROM TRADE WHERE TRADE_ID=";
  command += to_str(trade_id);
  command += ";";
  try{
    work W(*C);
    W.exec(command.c_str());
    W.commit();
  }catch(const std::exception &e){
    cerr <<"delete_trade: "<< e.what() <<std::endl;
    return -1;
  }
  return 0;
}
int update_account(connection *C,long account_id,double money){
  string command = "UPDATE ACCOUNT SET BALANCE=BALANCE-";
  command += to_str(money);
  command+= " WHERE ACCOUNT_ID=";
  command += to_str(account_id);
  command +=";";
  try{
    work W(*C);
    W.exec(command.c_str());
    W.commit();
  }catch(const std::exception &e){
    cerr <<"delete_trade: "<< e.what() <<std::endl;
    return -1;
  }
  return 0;
}
int update_trade(connection *C, long trade_id,long num){
  string command = "UPDATE TRADE SET LEFT_NUM=LEFT_NUM-";
  command += to_str(num);
  command += " WHERE TRADE_ID=";
  command += to_str(trade_id);
  command +=";";
  try{
    work W(*C);
    W.exec(command.c_str());
    W.commit();
  }catch(const std::exception &e){
    cerr <<"update_trade: "<< e.what() <<std::endl;
    return -1;
  }
  return 0;
}

vector<string> query_left(connection *C,long trade_id){
  string command = "SELECT TRADE_ID,LEFT_NUM FROM TRADE WHERE TRADE_ID=";
  command+=to_str(trade_id);
  command+=";";
  vector<string> res;
  try{
    work W(*C);
    result r=W.exec(command.c_str());
    W.commit();
    int rows = 0,columns= 0;
    rows = r.size();
    if(rows)
      columns = r[0].size();	
    for(int i = 0; i < rows; i++){
      for(int j = 0; j<columns; j++){
	res.push_back(r[i][j].c_str());
      }
    }
  }catch(const std::exception &e){
    cerr <<"query_left: "<< e.what() <<std::endl;
    //return;
  }
  return res;
}


vector<vector<string> > query_record(connection *C, long trade_id){
  string command = "SELECT * FROM RECORD WHERE TRADE_ID=";
  command+=to_str(trade_id);
  command+=";";
  vector<vector<string> >res;
  int price = 0;
  try{
    work W(*C);
    result r = W.exec(command.c_str());
    W.commit();
    int rows = 0,columns= 0;
    rows = r.size();
    if(rows){
      columns = r[0].size();
      res.resize(rows);
      for(int i = 0; i < rows; i++){
	for(int j = 1; j<columns; j++){
	  res[i].push_back(r[i][j].c_str());
	}
      }
    }
  }catch(const std::exception &e){
    cerr <<"query_record: "<< e.what() <<std::endl;
    //return;
  }
  
  res.insert(res.begin(),query_left(C,trade_id));
  return res;
}

vector<vector<string> > query_trade(connection *C, string sym,int top, double price){
  string command = "SELECT * FROM TRADE WHERE ID IN";
  command +=" (SELECT ID FROM TRADE WHERE SYM=";
  command +=format(sym);
  command.pop_back();
  command +=" AND PRICE";
  if(price>=0)
    command +=">=";
  else
    command +="<=";
  command+=to_str(price);
  command+=" ORDER BY PRICE ";
  if(price>=0)
    command +="DESC,TRADE_ID ASC)";
  else
    command +="ASC,TRADE_ID ASC)";
  command +=" LIMIT ";
  command += to_str(top);
  command +=";";
  vector<vector<string> >res;
  try{
    work W(*C);
    result r = W.exec(command.c_str());
    W.commit();
    int rows = 0,columns= 0;
    rows = r.size();
    res.resize(rows);
    if(rows){
      columns = r[0].size();
      for(int i = 0; i < rows; i++){
	for(int j = 1; j<columns; j++){
	  res[i].push_back(r[i][j].c_str());
	}
      }
    }
  }catch(const std::exception &e){
    cerr <<"query_trade: "<< e.what() <<std::endl;
    //return;
  }
  
  return res;
}

