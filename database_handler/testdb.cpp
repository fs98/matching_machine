#include <iostream>
#include <pqxx/pqxx>
#include <fstream>
#include <string>
#include <sstream>

#include "db_handler.h"

using namespace std;
using namespace pqxx;
int main (int argc, char *argv[]) 
{

  //Allocate & initialize a Postgres connection object
  connection *C;

  try{
    //Establish a connection to the database
    //Parameters: database name, user name, user password
    C = new connection("dbname=match_db user=postgres password=passw0rd");
    if (C->is_open()) {
      cout << "Opened database successfully: " << C->dbname() << endl;
    } else {
      cout << "Can't open database" << endl;
      return 1;
    }
  } catch (const std::exception &e){
    cerr <<"connect: "<< e.what() << std::endl;
    return 1;
  }
  const char *sql;
  string pre=
    "DROP TABLE IF EXISTS RECORD;"
    "DROP TABLE IF EXISTS TRADE;"
    "DROP TABLE IF EXISTS ACCOUNT";

  string set=
    "CREATE TABLE ACCOUNT("
    "ACCOUNT_ID INT PRIMARY KEY  NOT NULL,"
    "BALANCE               FLOAT NOT NULL DEFAULT 0);"
    "CREATE TABLE TRADE("
    "ID INT PRIMARY KEY  NOT NULL,"
    "TRADE_ID            INT NOT NULL UNIQUE,"
    "SYM                TEXT NOT NULL,"
    "ACCOUNT_ID          INT NOT NULL,"
    "LEFT_NUM            INT NOT NULL,"
    "PRICE             FLOAT NOT NULL,"
    "FOREIGN KEY (ACCOUNT_ID) REFERENCES ACCOUNT(ACCOUNT_ID));"
    "CREATE TABLE RECORD("
    "RECORD_ID INT PRIMARY KEY  NOT NULL,"
    "TRADE_ID            INT NOT NULL,"
    "SYM                TEXT NOT NULL,"
    "NUM                 INT NOT NULL,"
    "PRICE             FLOAT NOT NULL,"
    "TIME                INT NOT NULL);";;
  try{
    work W(*C);
    result r = W.exec(pre.c_str());
    W.commit();
  }catch(const std::exception &e){
    cerr <<"drop: "<< e.what() <<std::endl;
    return 1;
  }
  try{
    work W(*C);
    result r = W.exec(set.c_str());
    W.commit();
  }catch(const std::exception &e){
    cerr <<"create: "<< e.what() <<std::endl;
    return 1;
  }
  cout<<add_account(C,153245635,123.5)<<endl;
  cout<<add_account(C,765134654,123.5)<<endl;
  add_trade(C,1,"iphone",153245635,15,299);
  add_trade(C,2,"iphone",153245635,15,399);
  add_trade(C,3,"iphone",153245635,15,499);
  delete_trade(C,2);
  update_account(C,153245635,3.5);
  update_trade(C,1,5);
  vector<vector<string> >a = query_trade(C,"iphone",2,299);
  add_record(C,1,"iphone6",5,311,1223344);
  vector<vector<string> >b = query_record(C,1);
  cout<<"a"<<endl;
  for(int i = 0; i < a.size();i++){
    for(int j = 0; j<a[i].size();j++){
      cout<<a[i][j]<<" ";
    }
    cout<<endl;
  }
  cout<<"b"<<endl;
  for(int i = 0; i < b.size();i++){
    for(int j = 0; j<b[i].size();j++){
      cout<<b[i][j]<<" ";
    }
    cout<<endl;
  }
  
  C->disconnect();
  return 0;
}
