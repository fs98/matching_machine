#include <iostream>
#include <pqxx/pqxx>
#include <string>
#include <vector>
#include <sstream>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

using namespace std;
using namespace pqxx;
using namespace boost::property_tree;
//create a new account
int add_account(connection *C,long account_id, double balance);
//create a new record
int add_record(connection *C, long trade_id, string sym, long num, double price, long time);
//create a new trade, sell or buy
int add_trade(connection *C, long trade_id, string sym, long account_id,  long num,  double price);
//delete a trade
int delete_trade(connection *C, long trade_id);
//update the account, money is the money spent
int update_account(connection *C,long account_id,double money);
//update the trade, num is the num of sym that is closed
int update_trade(connection *C, long trade_id,long num);
//get the record with the trade_id, if trade is not finish, the first vector<string> contains the left num
vector<vector<string> > query_record(connection *C,  long trade_id);
//get the trade, top is the number of trade to get, price is the given price (can be positive or negative
vector<vector<string> > query_trade(connection *C, string sym,int top, double price);

//ptree transaction(connection *C, string name);

